package com.inspirethis.mike.androidtestdemo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricDataBindingTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21, manifest = ".src/main/AndroidManifest.xml")
public class DemoUnitTest {

    MainActivity mActivity;

    @Before
    public void setUp() throws Exception {
        mActivity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .get();
    }

    @After
    public void tearDown() throws Exception {
        Robolectric.reset();
        mActivity = null;
    }

    @Test
    public void myActivityAppearsAsExpectedInitially() {
        Assert.assertNotNull(mActivity);
    }

    @Test
    public void isEven_isCorrect() throws Exception {
        Assert.assertFalse("should return false", mActivity.isEven(3, 7));
        Assert.assertTrue("should return true", mActivity.isEven(1, 2));
        Assert.assertTrue("should return true", mActivity.isEven(4, 2));
        Assert.assertTrue("should return true", mActivity.isEven(0, 0));
    }

    @Test
    public void isNumericString_isCorrect() throws Exception {
        Assert.assertFalse("should return false", mActivity.isNumericString("A47"));
        Assert.assertFalse("should return false", mActivity.isNumericString(""));
        Assert.assertTrue("should return true", mActivity.isNumericString("47"));
    }


}